/**
 * @brief 
 * 
 * @file test_upload.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-11
 */
#include "test_upload.h"
#include "test_uart.h"
#include "user_app_log.h"
#include "ble_trans.h"
#include "HexStr.h"

static  char const success[] = "SUCCESS\0";
static  char const fail[] = "FAILED\0";

bool test_upload_data(void const *p_buf, uint8_t len, test_inter_type type)
{
    if(p_buf ==NULL || len == 0 || type >= TEST_INTER_MAX)
    {
        return false;
    }
    uint8_t *buf = (uint8_t *)p_buf;
    bool ret = false;
    if(type == TEST_INTER_BLE)
    {
        //BT_Send(0, buf,len);
        ret = ble_s_pkg_send(buf,len); 
        DBG_LOG("ble send: %s",buf);
    }
    else if(type == TEST_INTER_UART)
    {
        
        //send through uart interface
        ret = uart_pkg_send(buf,len);
        if(ret == 0)
        {
            DBG_LOG("uart send failed",buf);           
        }       
        DBG_LOG("%s",buf);
    }
    return ret;
}

/**
 * @brief 
 * 
 * @param type 
 */
void test_answer_ok(test_inter_type type)
{
    test_upload_data(success,7,type);
}
/**
 * @brief 
 * 
 * @param type 
 */
void test_answer_err(test_inter_type type)
{
    test_upload_data(fail,4,type);
}
