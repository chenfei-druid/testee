/**
 * @brief 
 * 
 * @file simple.c
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "dev_id.h"
#include "user_app_log.h"
#include "test_pb_callback.h"
#include "test_simple.pb.h"
#include "simple.h"
#include "user_data_pkg.h"
#include "user_app_identity.h"

#define SIMPLE_VERIRY 1
#if SIMPLE_VERIRY
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 */
static void verify_rsp_encode(unsigned char * const buf, uint32_t len)
{
    protocol_simple_rsp_t rsp_verify;
    char dev_id[20] = "\0";
    memset(dev_id,0,sizeof(dev_id));

    rsp_verify.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.Head.DeviceID.arg = dev_id;

    rsp_verify.DevId.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.DevId.arg          = dev_id;
 
    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_simple_rsp_fields,&rsp_verify);
    if(res)
    {

    }
}
#endif  //SIMPLE_VERIRY
static int32_t encode(uint8_t * const rsp_buf, uint32_t size,protocol_status_type_t status)
{

    protocol_simple_rsp_t rsp;
    char dev_id[15];
    memset(dev_id,0,sizeof(dev_id));

    rsp.has_Head = 0;
    if(rsp.has_Head)
    {
        app_get_indetity_msg(&rsp.Head);
        app_iden_get_device_id_char(dev_id);
        rsp.Head.MsgIndex = 0;

        rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
        rsp.Head.DeviceID.arg          = dev_id;
    }
    rsp.DevId.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.DevId.arg          = dev_id;

    rsp.Status             = status;

    pb_ostream_t            m_stream ;  
    bool res;  

    m_stream = pb_ostream_from_buffer(rsp_buf,size);
    res = pb_encode(&m_stream,protocol_simple_rsp_fields,&rsp);

    if(res)
    {
        DBG_LOG("simple response encode successed\r\n");
    #if SIMPLE_VERIRY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("simple response encode failed\r\n");
        return 0;
    }
    
}
/**
 * @brief 
 * 
 * @param ret 
 * @param chn 
 * @return uint32_t 
 */
uint32_t simple_response(protocol_status_type_t status, 
                         protocol_msg_type_t cmd_rsp_type,
                         test_inter_type chn)
{  
    uint8_t pb_buf[50];
    uint8_t send_buf[50+12];

    pkg_prot_frame_t p_prot_frame_t;
    int32_t len = 0;
    DBG_LOG("encode simple response message\r\n");
    len = encode(pb_buf,sizeof(pb_buf),status);
    DBG_LOG("encode len = %d\r\n",len);
    if(len <= 0)
    {
        return 0;
    }
    DBG_LOG("package simple response message\r\n");
    len = user_add_head_to_protbuf(&p_prot_frame_t,pb_buf,len,cmd_rsp_type);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("upload simple message, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("upload simple message over!\r\n",len);
    return len;
}


