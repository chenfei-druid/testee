

/**
 * @brief 
 * 
 * @file testee_lora.c
 * @date 2018-08-14
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_lora.pb.h"
#include "testee_lora.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"
#include "hal_cfg.h"

#define LORA_CHIP_ENABLE 0
#define UART_LORA_ENABLE 1

#if     (UART_LORA_ENABLE == 1)
#define UART_LORA_PROJECT   1  
#elif   (LORA_CHIP_ENABLE == 1)
#define LORA_CHIP_PROJECT   1
#endif

#define SF_MAX      12
#define SF_MIN      6

static bool lora_start(void)
{
    //if function is at firmwire, startup lora module at here
    bool ret;
#if LORA_ENABLE
    ret = lora_power_on();
#else
    ret = true;
#endif
    return ret;
}
static bool frq_set(uint32_t frq)
{
    //set lora carrier frequency at here, call lora driver interface
    bool ret;
#if LORA_ENABLE
    ret = lora_frq_set(frq);
#else
    ret = true;
#endif
    return ret;
}
static bool pwr_set(uint32_t pwr)
{
    //set lora TX power at here, call lora driver interface
    bool ret;
#if LORA_ENABLE
    ret = lora_pwr_set(pwr);
#else
    ret = true;
#endif
    return ret;
}
//#if UART_LORA_PROJECT
/**
 * @brief lora_speed_set
 * the speed set for integration(uart-lora) module project, not used to chip(lora) project.
 * and the speed means symbol rate of transmition, it must be converted to register data  
 * at lora driver.
 * @param speed 
 * @return true 
 * @return false 
 */
static bool speed_set(uint32_t speed)
{
    //set lora speed at here, call lora driver interface
    bool ret;
#if LORA_ENABLE
    ret = lora_speed_set(speed);
#else
    ret = true;
#endif
    return ret;
}
//#endif //UART_LORA_PROJECT

//#if LORA_CHIP_PROJECT
/*********************************************************************************
 if use chip project, the transmition speed is depend on Spreading Factor, BandWidth 
 and Code Rate, so set them independently.
 -------------------------------------------
 C/R = 4/5  |    BW = 62.5   |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |-------------------------------
            |    BW = 125    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |--------------------------------
            |    BW = 250    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |--------------------------------
            |    BW = 512    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
---------------------------------------------
 C/R = 4/6  |    BW = 62.5   |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |-------------------------------
            |    BW = 125    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |--------------------------------
            |    BW = 250    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
            |--------------------------------
            |    BW = 512    |    SF = 6
            |                |    SF = 7
            |                |    SF = 8
            |                |    SF = 9
            |                |    SF = 10
            |                |    SF = 11
            |                |    SF = 12
----------------------------------------------
*********************************************************************************/

/**
 * @brief lora_sf_set
 * 
 * @param sf 
 * sf = 6~12
 * @return true 
 * @return false 
 */
static bool sf_set(uint32_t sf)
{
    //set lora spreading factor at here, call lora driver interface
    if((sf < 6) || (sf > 12))
    {
        DBG_LOG("Invalid sf value %d\r\n",sf);
    }
    bool ret;
#if LORA_ENABLE
    ret = lora_sf_set(sf);
#else
    ret = true;
#endif
    return ret;
}
/**
 * @brief lora_bw_set
 * 
 * @param bw 
 * @return true 
 * @return false 
 */
static bool bw_set(uint32_t bw)
{
    //set lora BandWidth at here, call lora driver interface
    if((bw < 62) || (bw > 512))
    {
        DBG_LOG("Invalid BandWidth value %d\r\n",bw);
    }
    bool ret;
#if LORA_ENABLE
    ret = lora_bw_set(bw);
#else
    ret = true;
#endif
    return ret;
}
/**
 * @brief lora_cr_set
 * 
 * @param cr  5 = 4/5, 6 = 4/6
 * @return true 
 * @return false 
 */
static bool cr_set(uint32_t cr)
{
    //set lora BandWidth at here, call lora driver interface
    if((cr < 5) || (cr > 6))
    {
        DBG_LOG("Invalid cr value %d\r\n",cr);
    }
    bool ret;
#if LORA_ENABLE
    ret = lora_cr_set(cr);
#else
    ret = true;
#endif
    return ret;
}
static bool sleep(void)
{
    bool ret;
#if LORA_ENABLE
    ret = lora_sleep();
#else
    ret = true;
#endif
    return ret;
}
static bool loop_back(uint32_t dt)
{
    bool ret;
#if LORA_ENABLE
    ret = lora_loop_back();
#else
    ret = true;
#endif
    return ret;
}
//#endif
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_lora_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool ret;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    ret = pb_decode(&i_stream,protocol_lora_req_fields,p_pro_req); 
    DBG_LOG("command lora test decode %s\r\n",ret?"successed":"failed");
    return ret;
}
static uint32_t rsp_encode(uint8_t *rsp_buf,
                           uint32_t max_size,
                           protocol_status_type_t status)
{
    protocol_lora_rsp_t rsp;
    
    char dev_id[15] ="12345abcd\0"; //for tes

    memset(dev_id,0,sizeof(dev_id));
    memset(rsp_buf,0,sizeof(rsp_buf));

    app_get_indetity_msg(&rsp.Head);
    app_iden_get_device_id_char(dev_id);

    rsp.has_Head = 1;
    rsp.Head.MsgIndex = 13;

    rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.Head.DeviceID.arg          = dev_id;

    rsp.Status             = status;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(rsp_buf,max_size);
    bool res = pb_encode(&m_stream,protocol_lora_rsp_fields,&rsp);
    if(res)
    {
        DBG_LOG("device data response encode successed\r\n");
    #if DEV_DT_VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("device data response encode failed\r\n");
        return 0;
    }
}
static uint32_t response(protocol_status_type_t status,
                         test_inter_type chn)
{
    unsigned char rsp_buf[200];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    len = rsp_encode(rsp_buf,sizeof(rsp_buf),status);
    if(len <= 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[65];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_LORA_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("lora test response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("lora test response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param cmd_type  
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_lora_req_t *cmd,test_inter_type chn)
{
    
    protocol_status_type_t status;
    protocol_lora_type_t cmd_type = cmd->Type;
    bool ret;
    switch(cmd_type)
    {
        case PROTOCOL_LORA_TYPE_START:
            DBG_LOG("lora test: start lora module\r\n");
            ret = lora_start();
            break;
        case PROTOCOL_LORA_TYPE_FRQ_SET:
            DBG_LOG("lora test: set carrier frequency\r\n");
            ret = frq_set(cmd->Data);
            break;
        case PROTOCOL_LORA_TYPE_POWER_SET:
            DBG_LOG("lora test: set power\r\n");
            ret = pwr_set(cmd->Data);
            break;
    //#if UART_LORA_PROJECT
        case PROTOCOL_LORA_TYPE_SPEED_SET:
            DBG_LOG("lora test: set speed\r\n");
            ret = speed_set(cmd->Data);
            break;
    //#endif //UART_LORA_PROJECT
    //#if LORA_CHIP_PROJECT
        case PROTOCOL_LORA_TYPE_SF_SET:
            DBG_LOG("lora test: set spreading factor\r\n");
            ret = sf_set(cmd->Data);
            break;
        case PROTOCOL_LORA_TYPE_BW_SET:
            DBG_LOG("lora test: set BandWidth\r\n");
            ret = bw_set(cmd->Data);
            break;
        case PROTOCOL_LORA_TYPE_CR_SET:
            DBG_LOG("lora test: set C/R\r\n");
            ret = cr_set(cmd->Data);
            break;
        case PROTOCOL_LORA_TYPE_SLEEP_SET:
            DBG_LOG("lora test: let lora module sleep\r\n");
            ret = sleep();
            break;
        case PROTOCOL_LORA_TYPE_LOOP_BACK:
            DBG_LOG("lora test: random data loop back test\r\n");
            ret = loop_back(cmd->Data);
            break;        
    //#endif //LORA_CHIP_PROJECT
        default:
            DBG_LOG("lora test: invalid operate type = %d\r\n",cmd_type);
            break;
    }
    status = ret?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
    response(status,chn);

    return ret;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool lora_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_lora_req_t cmd;
    if(cmd_decode(&cmd,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  

    ret = cmd_handler(&cmd,chn);
    return ret;
}

#if LORA_VERIFY
static void verify_req(void)
{
    protocol_lora_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    //cmd.Type = PROTOCOL_LORA_TYPE_START;
    //cmd.Type = PROTOCOL_LORA_TYPE_FRQ_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_POWER_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_SPEED_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_SF_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_BW_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_CR_SET;
    //cmd.Type = PROTOCOL_LORA_TYPE_SLEEP_SET;
    cmd.Type = PROTOCOL_LORA_TYPE_LOOP_BACK;

    cmd.has_Data = 1;
    cmd.Data     = 0xaa;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_lora_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_LORA_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{

    verify_req();

}
#endif  //CHN_VERIFY
