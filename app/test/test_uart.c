/**
 * @brief 
 * 
 * @file test_uart.c
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-12
 */
#include "test_uart.h"
#include "hal_uart.h"
#include "nrfx_uarte.h"
#include "user_app_log.h"
#include "test_implement.h"


static  hal_uart_t      *uart = NULL;


/**
 * @brief 
 * 
 */
void test_uart_init(void)
{
    hal_uart_cfg_t uart_config;

    uart_config.baudrate = HAL_UART_BAUDRATE_115200;
    uart_config.parity   = HAL_UART_PARITY_NONE;
    uart_config.rx_buf_size = UART_RX_BUF;
    uart_config.rx_mode = HAL_UART_RX_MODE_BUFFERED;
    uart_config.rx_pin = UART_RX_PIN;
    uart_config.tx_pin = UART_TX_PIN;
    uart_config.rx_timeout_ms = 10;
    uart_config.tx_buf_size = UART_TX_BUF;
    uart_config.tx_mode = HAL_UART_TX_MODE_NOCOPY;
    uart_config.tx_timeout_ms = 10;

    uart = hal_uart_get_instance(0);
    nrfx_uarte_t inst = *(nrfx_uarte_t *)uart->inst;
    DBG_LOG("uart->inst = %d",inst.drv_inst_idx);
    if(uart == NULL)
    {
        DBG_LOG("test uart hal_uart_get_instance failed.");
        return;
    }
    if(uart->ops->init(uart,&uart_config) != HAL_ERR_OK)
    {
        DBG_LOG("test uart init failed.");
        return;
    }
    if(uart->ops->set_rx_timeout(uart,UART_RX_TIMEOUT) != HAL_ERR_OK)
    {
        DBG_LOG("test uart set rx timeout failed.");
        return;
    }
    if(uart->ops->set_rx_enable(uart,1) != HAL_ERR_OK)
    {
        DBG_LOG("test uart set_rx_enable failed.");
        return;
    }
    DBG_LOG("test uart initialized ok.");
    uart->ops->write(uart,(uint8_t *)"uart\r\n",6);
}

/**
 * @brief  close uart current instrance
 * 
 */
void test_close_uart(void)
{
    uart->ops->deinit(uart);
}

uint32_t test_uart_trans(uint8_t * buf, uint8_t length)
{
    hal_err_t ret = uart->ops->write(uart,buf,length);
    if(ret < 0)
    {
        return 0;
    }
    return length;
}

uint32_t test_uart_receive(void * const buf, uint8_t max_size)
{
    if(buf == NULL)
    {
        DBG_LOG("test_uart_receive pointer is null.");
        return false;
    }
    uint8_t *rx_buf = buf;
    return uart->ops->read(uart,rx_buf,max_size);
}
/**
 * @brief 
 * 
 * @param buffer 
 * @param length 
 * @return uint32_t 
 */
static uint32_t send_one_split(const uint8_t * const buffer, uint16_t length)
{
    return test_uart_trans((uint8_t *)buffer, length);
}
/**
 * @brief uart_pkg_send
 * 
 * @param buffer 
 * @param length 
 * @return uint32_t 
 */
uint32_t uart_pkg_send(void * const buffer, uint16_t const length)
{
    if(length <= 0 || buffer == NULL)
    {
        return false;
    }
    uint16_t    len_cnt     = length;    
    //uint8_t     send_buff[UART_TX_BUF] ;   
    uint8_t     sub_pkg_len = 0;
    uint8_t    *p_data      = (uint8_t *)buffer;
    int32_t     ret = 0;
    DBG_LOG("uart package send ...\r\n");
    uint8_t start[2] = {0xEF,0xEF};
    uint8_t stop[3] = {0xFE,0xFE,0xFE};
    send_one_split(start,2);
    do
    {            
        //split the datas to sub package  to send
        //sub_pkg_len = user_get_sub_pkg_from_cache(&p_data[length-len_cnt],send_buff,len_cnt,length,UART_TX_BUF-1);
        if(len_cnt > UART_TX_BUF)
        {
            sub_pkg_len = UART_TX_BUF;
            len_cnt -= sub_pkg_len;
        }
        else
        {
            sub_pkg_len = len_cnt;
            len_cnt = 0;
        }
        ret = send_one_split(p_data,sub_pkg_len);
        if(ret < 0)
        {
            DBG_LOG("send failed length = %d",sub_pkg_len);
            break;
        }
        p_data += sub_pkg_len;
        DBG_LOG("remanin package len = %d\r\n",len_cnt);
    }while(len_cnt);  
    if(len_cnt == 0)
    {
        send_one_split(stop,3);
    } 
    DBG_LOG("uart package send complete\r\n");
    return (len_cnt == 0)?1:0;
}
/**
 * @brief uart_pkg_receive
 * 
 * @param buf 
 * @param size 
 * @param timeout 
 * @return int32_t 
 */
int32_t uart_pkg_receive(void const * const buf, uint16_t size, uint32_t timeout)
{
    int32_t len = 0;
    uint8_t rx_buf[UART_RX_BUF];
    uint8_t *p_buf = (uint8_t *)buf;
    int8_t cnt = 0;
    uint8_t start[2] = {0xEF,0xEF};
    uint8_t stop[] = {0xFE,0xFE,0xFE};
    uint32_t time = timeout / UART_RX_TIMEOUT;
    do
    {
        cnt = test_uart_receive(rx_buf,sizeof(rx_buf));
        if(cnt > 0)
        {
            DBG_LOG("uart submessage receive len = %d\r\n",cnt);
            if((len + cnt) > size)
            {
                break;
            }
            memcpy(p_buf+len,rx_buf,cnt);
            len += cnt;
            if((rx_buf[cnt-1] == stop[0]) && (rx_buf[cnt-2] == stop[1]) && (rx_buf[cnt-3] == stop[2]))
            {
                break;
            }
        }
    }while(--time);
    if(len > 0)
    {
        uint32_t i;
        uint32_t temp_len = len;
        len = 0;
        for( i=0;i<temp_len-5;i++)
        {
            if((p_buf[i] == start[0]) && (p_buf[i+1] == start[1]))
            {
                uint32_t k;               
                for(k=i+2;k<=temp_len-3;k++)
                {
                    if((p_buf[k] == stop[0]) && (p_buf[k+1] == stop[1]) && (p_buf[k+2] == stop[2]))
                    {
                        memcpy(p_buf,&p_buf[i+2],k-i);
                        len = k-i-2;
                        break;
                    }
                }
                break;
            }
        }
    }
    return len;
}
//end
