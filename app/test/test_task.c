/**
 * @brief 
 * 
 * @file test_task.c
 * @date 2018-07-31
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
/**--------------------------------------includes---------------------------------------------**/
#include <stdint.h>
#include "ble_trans.h"
#include "nrfx_gpiote.h"
#include "test_task.h"
#include "user_app_log.h"
#include "test_parse.h"
#include "test_uart.h"
#include "test_upload.h"
#include "ble_app.h"
#include "hal_uart.h"
#include "hal_pin.h"
#include "testee_bt_mode.h"
#include "hal_nrf_flash.h"

/**--------------------------------------define---------------------------------------------**/
#define RUN_INDEX   23
//#define CMD_INDEX   23

static  TaskHandle_t    test_handle = NULL;

void test_gpio_init(void)
{
    if(nrfx_gpiote_init() != NRFX_SUCCESS)
    {
        DBG_LOG("test gpio initialized failed.");
        return;
    }
    DBG_LOG("test gpio initialized ok.");
}

/**
 * @brief 
 * 
 * @param arg 
 */
void test_task_handle(void *arg)
{   
    DBG_LOG("+++++++++++++++++++ test task startup +++++++++++++++++++++");
    uint8_t cmd_buf[CMD_BUF] = {0};
    int32_t len = 0;
    test_gpio_init();
    test_uart_init();
    hal_pin_set_mode(RUN_INDEX,HAL_PIN_MODE_OUT);
    hal_pin_write(RUN_INDEX,HAL_PIN_LVL_LOW);
    user_nrf_flash_init();
    while(1)
    {
        len = 0;
        len = uart_pkg_receive(cmd_buf,sizeof(cmd_buf),500);
        if(len > 0)
        {
            hal_pin_write(RUN_INDEX,HAL_PIN_LVL_HIGH);
            DBG_LOG("uart received command , length = %d",len);
            for(uint8_t i=0;i<len;i++)
            {
                DBG_B("%02x ",cmd_buf[i]);
            }
            DBG_B("\r\n");
            test_parse_handle(cmd_buf,len);
            memset(cmd_buf,0,sizeof(cmd_buf));
            len = 0;
            hal_pin_write(RUN_INDEX,HAL_PIN_LVL_LOW);
        }
        len = ble_s_receive(cmd_buf,sizeof(cmd_buf),500);
        if(len > 0)
        {
            hal_pin_write(RUN_INDEX,HAL_PIN_LVL_HIGH);
            //when received data on ble , upload all data to PC at once
            DBG_LOG("ble slave received len = %d",len);
            test_parse_handle(cmd_buf,len);
            memset(cmd_buf,0,sizeof(cmd_buf));
            len = 0;       
            hal_pin_write(RUN_INDEX,HAL_PIN_LVL_LOW); 
        }
        //vTaskDelay(1000);
        hal_pin_write(RUN_INDEX,HAL_PIN_LVL_HIGH);
        vTaskDelay(50);
        hal_pin_write(RUN_INDEX,HAL_PIN_LVL_LOW);
    }
}
/**
 * @brief 
 * 
 */
void test_creat_task(void)
{
    uint32_t ret = xTaskCreate( test_task_handle, "test", 1024, NULL, 1, &test_handle );
    if(pdPASS != ret)
    {
        APP_ERROR_HANDLER(NRF_ERROR_NO_MEM);
    }  
}


