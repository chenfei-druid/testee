
#ifndef DEV_ID_H
#define DEV_ID_H

#include <stdint.h>


bool read_dev_id(char * const p_mac, uint32_t size);

#endif  //DEV_ID_H
