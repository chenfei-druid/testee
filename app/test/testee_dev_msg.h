
/**
 * @brief 
 * 
 * @file test_devmsg.h
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_DEVMSG_H
#define TEST_DEVMSG_H

#include "test_upload.h"
#include "testee_devmsg.pb.h"

#define DEV_MSG_VERIFY 0

typedef struct
{
    char  msg[32];
    protocol_status_type_t st;
    protocol_dev_msg_type_t type;

}dev_msg_t;

#if DEV_MSG_VERIFY
void pb_verify(void);
#endif //DEV_MSG_VERIFY

bool dev_msg_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_DEVMSG_H
