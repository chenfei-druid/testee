
/**
 * @brief 
 * 
 * @file testee_sn.h
 * @date 2018-08-13
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TESTEE_BT_MODE_H
#define TESTEE_BT_MODE_H

#include "test_upload.h"

#define BT_VERIFY 0

#if BT_VERIFY
void pb_verify(void);
#endif //BT_VERIFY

bool bt_mode_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TESTEE_BT_MODE_H
