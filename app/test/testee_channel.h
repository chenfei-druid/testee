
/**
 * @brief 
 * 
 * @file test_dev_data.h
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TESTEE_CHANNEL_H
#define TESTEE_CHANNEL_H

#include <stdint.h>
#include "test_upload.h"

#define CHN_VERIFY 0

#if CHN_VERIFY
void pb_verify(void);
#endif //CHN_VERIFY

bool channle_loopback_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TESTEE_CHANNEL_H
