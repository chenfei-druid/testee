/**
 * @brief 
 * 
 * @file simple.h
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef SIMPLE_H
#define SIMPLE_H

#include <stdint.h>
#include "test_upload.h"
#include "test_status.pb.h"
#include "test_define.pb.h"

#define VERIFY 0

#if VERIFY
void pb_verify(void);
#endif //VERIFY

uint32_t simple_response(protocol_status_type_t status, 
                         protocol_msg_type_t cmd_rsp_type,
                         test_inter_type chn);

#endif  //SIMPLE_H
