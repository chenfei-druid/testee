
/**
 * @brief 
 * 
 * @file testee_net.h
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TESTEE_POWER_H
#define TESTEE_POWER_H

#include <stdint.h>
#include "test_upload.h"
#include "testee_net.pb.h"

#define POWER_VERIFY 0


#if POWER_VERIFY
void pb_verify(void);
#endif //NET_VERIFY

bool power_set_hanler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TESTEE_POWER_H
