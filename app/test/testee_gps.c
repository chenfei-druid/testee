

/**
 * @brief 
 * 
 * @file testee_net.c
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_gps.pb.h"
#include "testee_gps.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"

#define GPS_CODE_VERIFY 1
/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
static bool enter(void)
{
    return true;
}
/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
static bool start_position(void)
{
    //call gps startup interface
    return true;
}
/**
 * @brief 
 * 
 * @return true 
 * @return false 
 */
static bool stop_position(void)
{
    //call gps stop interface
    return true;
}
static int32_t get_message(gps_msg_t *gps_msg)
{
    int32_t len = 0;
    // call gps message read interface
    return len;
}
/**
 * @brief 
 * 
 * @param p_pro_req
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_gps_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool ret;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    ret = pb_decode(&i_stream,protocol_gps_req_fields,p_pro_req); 
    DBG_LOG("command gps test decode %s\r\n",ret?"successed":"failed");
    return ret;
}
#if GPS_CODE_VERIFY
static void verify_rsp_encode(unsigned char * const buf, uint32_t len)
{
    protocol_gps_rsp_t rsp_verify;
    char dev_id[20] = "\0";
    memset(dev_id,0,sizeof(dev_id));

    rsp_verify.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.Head.DeviceID.arg = dev_id;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_gps_rsp_fields,&rsp_verify);
    if(res)
    {

    }
}
#endif //GPS_CODE_VERIFY
static uint32_t rsp_encode(uint8_t *rsp_buf,
                           uint32_t max_size,
                           protocol_gps_rsp_t *rsp)
{
    rsp->has_Head = 0;
    if(rsp->has_Head)
    {
        char dev_id[15] ="12345abcd\0"; //for tes
        memset(dev_id,0,sizeof(dev_id));
        app_get_indetity_msg(&rsp->Head);
        app_iden_get_device_id_char(dev_id);

        rsp->Head.MsgIndex = 13;

        rsp->Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
        rsp->Head.DeviceID.arg          = dev_id;
    }
    //rsp->Status  = gps_rsp.res.st;
    //rsp->CmdType = gps_rsp.res.cmd;

    pb_ostream_t  m_stream;    
    m_stream = pb_ostream_from_buffer(rsp_buf,max_size);
    bool res = pb_encode(&m_stream,protocol_gps_rsp_fields,rsp);
    if(res)
    {
        DBG_LOG("gps test response encode successed\r\n");
    #if GPS_CODE_VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // GPS_CODE_VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("gps test response encode failed\r\n");
        return 0;
    }
}

static uint32_t response(protocol_gps_rsp_t *gps_rsp,
                         test_inter_type chn)
{
    unsigned char rsp_buf[200];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    len = rsp_encode(rsp_buf,sizeof(rsp_buf),gps_rsp);
    if(len <= 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[220];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_GPS_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("gps test response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("gps test response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param cmd_type  
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_gps_req_t *cmd,test_inter_type chn)
{   
    //gps_res_t gps_res;
    gps_msg_t gps_msg;
    protocol_gps_rsp_t proto_gps_rsp;
    //gps_rsp_t gps_rsp;
    protocol_gps_cmd_type_t cmd_type = cmd->CmdType;
    bool ret;
    memset(&proto_gps_rsp,0,sizeof(proto_gps_rsp));
    switch(cmd_type)
    {
        case PROTOCOL_GPS_CMD_TYPE_ENTER_MODE:
            DBG_LOG("gps test: enter gps test mode\r\n");
            ret = enter();
            proto_gps_rsp.CmdType = PROTOCOL_GPS_CMD_TYPE_ENTER_MODE;
            break;
        case PROTOCOL_GPS_CMD_TYPE_START_POSITION:
            DBG_LOG("gps test: startup position\r\n");
            ret = start_position();
            proto_gps_rsp.CmdType = PROTOCOL_GPS_CMD_TYPE_START_POSITION;
            //ret_rand = (cmd->has_Random)?(~cmd->Random):0xffff;
            break;
        case PROTOCOL_GPS_CMD_TYPE_STOP_POSITION:
            DBG_LOG("gps test: stop position\r\n");
            ret = stop_position();
            proto_gps_rsp.CmdType = PROTOCOL_GPS_CMD_TYPE_STOP_POSITION;
            break;
        case PROTOCOL_GPS_CMD_TYPE_GET_CNR:
            DBG_LOG("gps test: get CNR message\r\n");
            ret = get_message(&gps_msg);
            proto_gps_rsp.CmdType = PROTOCOL_GPS_CMD_TYPE_GET_CNR;
            proto_gps_rsp.has_Latitude  = 1;
            proto_gps_rsp.Latitude      = gps_msg.Latitude;
            proto_gps_rsp.has_Longitude = 1;
            proto_gps_rsp.Longitude     = gps_msg.Longitude;
            proto_gps_rsp.has_Quality   = 1;
            proto_gps_rsp.Quality       = gps_msg.Quality;
            proto_gps_rsp.has_CNR       = 1;
            proto_gps_rsp.CNR           = gps_msg.CNR;
            break;    
        default:
            DBG_LOG("gps test: invalid gps command type\r\n");
            break;
    }
    proto_gps_rsp.Status = ret?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;

    response(&proto_gps_rsp,chn);
    return ret;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool gps_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_gps_req_t cmd;
    if(cmd_decode(&cmd,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    ret = cmd_handler(&cmd,chn);
    return ret;
}

#if GPS_VERIFY
static void verify_req(void)
{
    protocol_gps_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];
    char str[200];
    uint8_t send_buf[60];
    int32_t len = 0;

    pkg_prot_frame_t p_prot_frame_t;
    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    cmd.CmdType = PROTOCOL_GPS_CMD_TYPE_ENTER_MODE;
    //cmd.CmdType = PROTOCOL_GPS_CMD_TYPE_START_POSITION;
    //cmd.CmdType = PROTOCOL_GPS_CMD_TYPE_STOP_POSITION;
    //cmd.CmdType = PROTOCOL_GPS_CMD_TYPE_GET_CNR;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_gps_req_fields,&cmd);
    if(res)
    {
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_GPS_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{

    verify_req();

}
#endif  //CHN_VERIFY
