
/**
 * @brief 
 * 
 * @file testee_lora.h
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_LORA_H
#define TEST_LORA_H

#include <stdint.h>
#include "test_upload.h"
#include "testee_devdata.pb.h"

#define LORA_VERIFY 0
/*
typedef struct
{
    uint32_t dt;
    protocol_status_type_t st;
    protocol_cmd_type_t type;
}lora_rsp_t;
*/
#if LORA_VERIFY
void pb_verify(void);
#endif //LORA_VERIFY

bool lora_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_LORA_H
