

/**
 * @brief 
 * 
 * @file testee_power.c
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_power.pb.h"
#include "testee_power.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"
#include "hal_cfg.h"

//static int32_t power = 0;
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_pwr_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool ret;
    pb_istream_t i_stream ;
    char id[15];

    memset(&i_stream,0,sizeof(i_stream));
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    ret = pb_decode(&i_stream,protocol_pwr_req_fields,p_pro_req); 
    DBG_LOG("command power set decode %s\r\n",ret?"successed":"failed");
    return ret;
}
static bool set_pwr(protocol_pwr_type_t pwr)
{
    if(pwr > PROTOCOL_PWR_TYPE_MAX)
    {
        DBG_LOG("Given power value %d is invalid\r\n",pwr);
        return false;
    }
    DBG_LOG("Set power to %s\r\n",(pwr==PROTOCOL_PWR_TYPE_MIN)?"PROTOCOL_PWR_TYPE_MIN":"PROTOCOL_PWR_TYPE_MAX");
    //power = pwr;
    bool ret = false;
    switch(pwr)
    {
        case PROTOCOL_PWR_TYPE_MIN:
            DBG_LOG("Set power to min\r\n");
            #if PWR_SET
            ret = set_power_min();
            #else
            ret = true;
            #endif
            break;
        case PROTOCOL_PWR_TYPE_MAX:
            DBG_LOG("Set power to max\r\n");
            #if PWR_SET
            ret = set_power_max();
            #else
            ret = true;
            #endif
            break;
        default:
            break;
    }
    return ret;
}
/**
 * @brief 
 * 
 * @param cmd_type  
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_pwr_req_t *cmd,test_inter_type chn)
{
    
    protocol_status_type_t status;
    bool ret;

    DBG_LOG("set power porecess\r\n");

    ret = set_pwr(cmd->Type);

    status = ret?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
    simple_response(status,PROTOCOL_MSG_TYPE_POWER_RSP,chn);
    DBG_LOG("set power over\r\n");
    return ret;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool power_set_hanler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_pwr_req_t cmd;
    if(cmd_decode(&cmd,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    ret = cmd_handler(&cmd,chn);
    return ret;
}

#if POWER_VERIFY
static void verify_req(void)
{
    protocol_pwr_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    //cmd.Type = PROTOCOL_PWR_TYPE_MIN;
    cmd.Type = PROTOCOL_PWR_TYPE_MAX;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_pwr_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_POWER_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{

    verify_req();

}
#endif  //POWER_VERIFY
