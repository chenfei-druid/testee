
/**
 * @brief 
 * 
 * @file test_dev_data.h
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_DEV_DATA_H
#define TEST_DEV_DATA_H

#include <stdint.h>
#include "test_upload.h"
#include "testee_devdata.pb.h"

#define DEV_DT_VERIFY 1

typedef struct
{
    uint32_t dt;
    protocol_status_type_t st;
    protocol_dev_data_type_t type;

}dev_data_t;

#if DEV_DT_VERIFY
void pb_verify(void);
#endif //DEV_DT_VERIFY

bool dev_data_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_DEVMSG_H
