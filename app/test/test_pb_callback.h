

bool user_app_encode_repeated_var_string(pb_ostream_t *stream, const pb_field_t *fields,  void * const *str);
bool user_app_encode_repeated_var_int(pb_ostream_t *stream, const pb_field_t *fields,  void * const *str);
bool user_app_decode_repeated_var_string(pb_istream_t *stream, const pb_field_t *field, void **str);

