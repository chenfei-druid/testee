

/**
 * @brief 
 * 
 * @file testee_bt_mode.c
 * @date 2018-08-13
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_channel.pb.h"
#include "testee_channel.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_test_chn_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_test_chn_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command channel loopback decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command channel loopback decode failed\r\n");
        return false;
    }  
}
static bool rsp_encode(uint8_t *rsp_buf,uint32_t max_size,uint32_t random_value )
{
    protocol_test_chn_rsp_t rsp;
    
    char dev_id[15];

    
    memset(dev_id,0,sizeof(dev_id));

    app_get_indetity_msg(&rsp.Head);
    app_iden_get_device_id_char(dev_id);

    rsp.has_Head = 0;
    rsp.Head.MsgIndex = 0;

    rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.Head.DeviceID.arg          = dev_id;

    rsp.Random              = random_value;

    pb_ostream_t            m_stream ;  
    bool ret;  

    m_stream = pb_ostream_from_buffer(rsp_buf,max_size);
    ret = pb_encode(&m_stream,protocol_test_chn_rsp_fields,&rsp);

    if(ret)
    {
        DBG_LOG("channel loop back response encode successed\r\n");
    #if VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("channel loop back response encode failed\r\n");
        return 0;
    }
}
static bool response(uint32_t random_value ,test_inter_type chn)
{
    unsigned char rsp_buf[100];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    random_value = ~random_value;
    DBG_LOG("response random_value = 0x%x(%d)\r\n",random_value,random_value);
    len = rsp_encode(rsp_buf,sizeof(rsp_buf),random_value);
    if(len <= 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[65];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_CHANNEL_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("channel loop back response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("channel loop back response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param cmd_type  
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(uint32_t random,test_inter_type chn)
{
    DBG_LOG("received random_value = 0x%x(%d)\r\n",random,random);

    response(random,chn);
    return true;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool channle_loopback_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_test_chn_req_t cmd;
    if(cmd_decode(&cmd,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  

    ret = cmd_handler(cmd.Random,chn);
    return ret;
}

#if CHN_VERIFY
static void verify_req(void)
{
    protocol_test_chn_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    cmd.Random = (uint32_t) rand();

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_test_chn_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_CHANNEL_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{

    verify_req();

}
#endif  //CHN_VERIFY