/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-12-1
** Version      :	    0.0
** Descriptions :		user_nrf_flash.c
*/

#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
//#include "softdevice_handler.h"
//#include "ble_flash.h"
#include "hal_nrf_flash.h"
#include "nrf_fstorage_sd.h"
#include "nrf_fstorage.h"
#include "app_error.h"

static volatile bool     fs_callback_flag;
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt);
static nrf_fstorage_info_t  m_flash_info = {.erase_unit = USER_PAGE_SIZE,  //in bytes
                                            .program_unit = sizeof(int32_t),
                                            .rmap = true,
                                            .wmap = false,            
                                            };
NRF_FSTORAGE_DEF(nrf_fstorage_t fstorage) =
{
    .p_flash_info = &m_flash_info,
    /* Set a handler for fstorage events. */
    .evt_handler = fstorage_evt_handler,
    //.evt_handler = NULL,

    /* These below are the boundaries of the flash space assigned to this instance of fstorage.
     * You must set these manually, even at runtime, before nrf_fstorage_init() is called.
     * The function nrf5_flash_end_addr_get() can be used to retrieve the last address on the
     * last page of flash available to write data. */
    .start_addr = USER_MEMORY_START,  
    .end_addr   = USER_MEMORY_END,
};

//static void user_save_current_position(uint32_t upload_addr, uint32_t save_addr);
typedef struct
{
    uint32_t    p_data_save;
    uint32_t    p_data_upload;
    uint32_t    data_save_length;
}user_data_head;
/**
* @function@name:    user_nrf_flash_event_handler
* @description  :    the call back of flash event
* @input        :    none
* @output       :    none
* @return       :    none
*/
static void fstorage_evt_handler(nrf_fstorage_evt_t * p_evt)
{
    if (p_evt->result == NRF_SUCCESS)
    {
        fs_callback_flag = 1;
    }
}

/**
* @function@name:    user_nrf_flash_init
* @description  :    Function for initializing flash on system
* @input        :    none
* @output       :    none
* @return       :    none
*/
void user_nrf_flash_init(void)
{
    ret_code_t rc;
    //fs_init();
    nrf_fstorage_api_t * p_fs_api = &nrf_fstorage_sd;
    rc = nrf_fstorage_init(&fstorage, p_fs_api, NULL);
    APP_ERROR_CHECK(rc);
}
/**
* @function@name:    user_nrf_flash_page_erase
* @description  :    Function for erase page on flash
* @input        :    page_start: pointor to page start address, page_num: page numbers will be erased
* @output       :    none
* @return       :    none
*/
bool user_nrf_flash_page_erase(uint32_t *page_start,int8_t page_num)
{
    if(page_num <= 0 || page_num > USER_PAGE_MAX)
    {
        return false;
    }
    fs_callback_flag = 0;
    //ret_code_t rc;
    nrf_fstorage_erase(&fstorage, (uint32_t)page_start, page_num, NULL);

	uint32_t retry = 5000000;
    while(fs_callback_flag == 0 && retry-- > 0);
    if(fs_callback_flag == 0)
    {
        return false;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_word_write
* @description  :    Function for write one word into flash memory
* @input        :    p_address: pointor to word data address
* @output       :    value: pointor to word data will be write
* @return       :    none
*/
bool user_nrf_flash_word_write(uint32_t * p_address, uint32_t value)
{    
    fs_callback_flag = 0;
    if((uint32_t)p_address >fstorage.end_addr || (uint32_t)p_address < fstorage.start_addr)
    {
        return false;
    }
    nrf_fstorage_write(&fstorage, (uint32_t)p_address, &value, sizeof(value), NULL);
    uint32_t retry = 5000000;
    while(fs_callback_flag == 0 && retry-- > 0);
    if(fs_callback_flag == 0)
    {
        return false;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_words_write
* @description  :    write some data as words into a searial continue place
* @input        :    p_address: the start address,
*                    word_buff: pointor to data will be writed, len: data length, one word = 4 byte
* @output       :    none
* @return       :    none
*/
bool user_nrf_flash_words_write(uint32_t * p_address, const uint32_t *const word_buff, const uint32_t len)
{
    for(uint32_t i=0;i<len;i++)
    {
        if(user_nrf_flash_word_write(p_address, word_buff[i]) == false)
        {
            return false;
        }
        p_address ++;
    }
    return true;
}
/**
* @function@name:    user_nrf_flash_word_read
* @description  :    Function for read one word from flash memory
* @input        :    p_address: pointor to word data address
* @output       :    value: pointor to cache of acquired word of word
* @return       :    none
*/
bool user_nrf_flash_word_read(uint32_t * p_address, uint32_t * value)
{    
    if((uint32_t)p_address == 0x00000000)
    {
        p_address = (uint32_t *)USER_MEMORY_START;
    }
    //*value = *p_address;
    if(nrf_fstorage_read(&fstorage, (uint32_t)p_address, (uint32_t *)value, 4) != NRF_SUCCESS)
    {
        return false;
    }
    return true;
    //memcpy(value, (uint32_t *)0x00074000, 1);
}

/**
* @function@name:    user_nrf_flash_read_words
* @description  :    Function for getting words from flash memory
* @input        :    p_address: pointor to data address, byte_len: word data length
* @output       :    data_buff: pointor to cache of acquired words of word
* @return       :    none
*/
bool user_nrf_flash_read_words(uint32_t * p_address, uint32_t * data_buff, uint32_t len)  
{
    for(uint32_t i=0;i<len;i++)
    {
        user_nrf_flash_word_read(p_address,&data_buff[i]);
        p_address ++;
    }
    return true;
}

