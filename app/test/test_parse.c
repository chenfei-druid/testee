/**
 * @brief 
 * 
 */
#include "test_parse.h"
#include "crc16.h"
#include "user_app_log.h"
#include "user_data_pkg.h"
#include "test_parse.h"
#include "test_define.pb.h"
#include "ble_app.h"
#include "test_upload.h"
#include "testee_dev_msg.h"
#include "testee_dev_data.h"
#include "testee_sn.h"
#include "testee_bt_mode.h"
#include "testee_channel.h"
#include "testee_speed.h"
#include "testee_lora.h"
#include "testee_net.h"
#include "testee_gps.h"
#include "testee_power.h"

typedef struct pkg_prot_frame_t pkg_prot_frame;

uint8_t test_check_sum(uint8_t *buf, uint16_t len, uint16_t *cs_v)
{
    if(buf == NULL)
    {
        DBG_LOG("test check sum pointer is null");
        return false;
    }
    if(len == 0)
    {
        DBG_LOG("test check sum length = 0");
        return false;
    }
    uint16_t cs = 0;
    if(cs_v != NULL)
    {
        cs = *cs_v;
    }
    for(uint16_t i=0;i<len;i++)
    {
        cs += buf[i];
    }
    return cs;
}
static test_inter_type get_cmd_chn(void)
{
    if(get_s_connect_status())
    {
        return TEST_INTER_BLE;
    }
    return TEST_INTER_UART;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param len 
 * @param type 
 * @return true 
 * @return false 
 */
bool test_parse_handle(uint8_t *p_cmd,uint8_t len)
{
    DBG_LOG("Testee command parsing......");
    if(p_cmd == NULL || len == 0)
    {
        DBG_LOG("test command pointer is null");
        return false;
    }
    bool ret = false;
    pkg_prot_frame_t *cmd_pkg = (pkg_prot_frame_t *)p_cmd;
    uint8_t *p_frot_data = (uint8_t *)(p_cmd+sizeof(pkg_prot_head_t));

    if(cmd_pkg->prot_head.frame_len != len - sizeof(pkg_prot_head_t))
    {
        DBG_LOG("test cmd length is wrong, give len = %d, but cacul len=%d",cmd_pkg->prot_head.frame_len, len - sizeof(pkg_prot_head_t));      
        return false;
    }

    uint16_t head_len = sizeof(pkg_prot_head_t);
    uint16_t give_crc16 = cmd_pkg->prot_head.crc16;
    uint16_t cacul_crc16 = crc16_compute(p_cmd,head_len-2,NULL);
    cacul_crc16 = crc16_compute(p_cmd+head_len,cmd_pkg->prot_head.frame_len,&cacul_crc16);
    if(give_crc16 != cacul_crc16)
    {
        DBG_LOG("test cmd crc16 is wrong, give_crc16=0x%x, cacul_crc16 = 0x%x",give_crc16, cacul_crc16);
        return false;
    }
    protocol_msg_type_t cmd_type = cmd_pkg->prot_head.cmd_type;
    DBG_LOG("test cmd code = %d", cmd_type);
    if(cmd_type > PROTOCOL_MSG_TYPE_MAX)
    {
        DBG_LOG("Invalid command");
        return false;
    }
    switch(cmd_type)
    {
        case PROTOCOL_MSG_TYPE_READ_DEV_DATA_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_READ_DEV_DATA_REQ");
            ret = dev_data_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_READ_DEV_MSG_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_READ_DEV_MSG_REQ");
            ret = dev_msg_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_SN_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_SN_REQ");
            ret = write_sn_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_BT_MODE_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_BT_MODE_REQ");
            ret = bt_mode_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_CHANNEL_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_CHANNEL_REQ");
            ret = channle_loopback_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_SPEED_SET_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_SPEED_SET_REQ");
            ret = set_speed_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_LORA_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_LORA_REQ");
            ret = lora_test_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_NET_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_NET_REQ");
            ret = net_test_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_GPS_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_GPS_REQ");
            ret = gps_test_handler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        case PROTOCOL_MSG_TYPE_POWER_REQ:
            DBG_LOG("PROTOCOL_MSG_TYPE_POWER_REQ");
            ret = power_set_hanler(p_frot_data,cmd_pkg->prot_head.frame_len,get_cmd_chn());
            break;
        default:
            DBG_LOG("the command is invalid");
            ret = false;
            break;
        //for local tester board as below
    }
    return ret;
}