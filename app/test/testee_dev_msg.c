
/**
 * @brief 
 * 
 * @file tester_selftest.c
 * @date 2018-07-31
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_dev_msg.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"
#include "record.h"
#include "testee_sn.h"
#include "hal_nrf_flash.h"
#include "hal_cfg.h"
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_dev_msg_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_dev_msg_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command devmsg decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command devmsg decode failed\r\n");
        return false;
    }  
}
#if 0
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 */
static void verify_rsp_encode(unsigned char * const buf, uint32_t len)
{
    protocol_dev_msg_rsp_t rsp_verify;
    char dev_id[20] = "\0";
    char msg[13] = "\0";
    memset(dev_id,0,sizeof(dev_id));

    rsp_verify.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.Head.DeviceID.arg = dev_id;

    rsp_verify.Msg.funcs.decode   = &user_app_decode_repeated_var_string;
    rsp_verify.Msg.arg            = (uint8_t *)msg;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_dev_msg_rsp_fields,&rsp_verify);
    if(res)
    {

    }
}
#endif
static uint32_t encode(uint8_t *rsp_buf,
                        uint16_t size,
                        dev_msg_t *rsp_msg)
{
    protocol_dev_msg_rsp_t rsp;

    

    rsp.has_Head = 0;
    if(rsp.has_Head)
    {
        char dev_id[15] ="12345abcd\0"; //for tes

        memset(dev_id,0,sizeof(dev_id));

        app_get_indetity_msg(&rsp.Head);
        app_iden_get_device_id_char(dev_id);
        rsp.Head.MsgIndex = 13;

        rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
        rsp.Head.DeviceID.arg          = dev_id;
    }
    rsp.Msg.funcs.encode   = &user_app_encode_repeated_var_string;
    rsp.Msg.arg            = (uint8_t *)rsp_msg->msg;
    
    rsp.Cmd = rsp_msg->type;
    rsp.Status             = rsp_msg->st;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(rsp_buf,size);
    bool res = pb_encode(&m_stream,protocol_dev_msg_rsp_fields,&rsp);
    if(res)
    {
        DBG_LOG("testee device message response encode successed\r\n");
    #if 0
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("testee device message response encode failed\r\n");
        return 0;
    }
}
/**
 * @brief 
 * 
 * @param ret 
 * @param chn 
 * @return uint32_t 
 */
static uint32_t response(dev_msg_t *rsp_msg,test_inter_type chn)
{
    unsigned char rsp_buf[80];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    DBG_LOG("response device msg = %s\r\n",rsp_msg->msg);
    len = encode(rsp_buf,sizeof(rsp_buf),rsp_msg);
    if(len < 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[120];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_READ_DEV_MSG_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("device data response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("device data response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param p_sn 
 * @param size 
 * @return true 
 * @return false 
 */
bool get_sn(dev_msg_t *dev_msg, uint32_t size)
{
    bool ret = 0;
#if DEV_SN
    ret = get_sn(dev_msg->msg,size);
#else
    sn_record_t sn;
    memset(&sn,0,sizeof(sn));
    //ret = Record_Read(RECORD_TYPE_SN,(uint8_t *)&sn,1);
    ret = user_nrf_flash_read_words((uint32_t *)USER_MEMORY_START,(uint32_t *)&sn,sizeof(sn_record_t));
    if(ret == true)
    {
        if(sn.len > sizeof(sn.sn))
        {
            sn.len = sizeof(sn.sn);
        }
        memcpy((uint8_t *)dev_msg->msg,sn.sn,sn.len);
    }
    //strcpy(p_sn,"sn_test\0");
#endif //DEV_SN
    dev_msg->st = (ret)? PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
    return ret;
}
/**
 * @brief 
 * 
 * @param p_mac 
 * @param size 
 * @return true 
 * @return false 
 */
bool get_mac(char * const p_mac, uint32_t size)
{
    uint8_t mac_hex[7];

    uint8_t len = get_device_mac_addr(mac_hex,sizeof(mac_hex));
    if(len == 0)
    {
        return false;
    }
    HexToStr(p_mac,mac_hex,len);
    DBG_LOG("read device mac: %s\r\n",p_mac);
    return true;
}
/**
 * @brief 
 * 
 * @param p_imei 
 * @param size 
 * @return true 
 * @return false
 */
bool get_imei(dev_msg_t *dev_msg, uint32_t size)
{
    bool ret; 
#if DEV_IMEI
    ret = read_imei(dev_msg->msg,size);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    strcpy(dev_msg->msg,"imei_test\0");
    dev_msg->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
    ret = true;
#endif //DEV_IMEI
    return ret;
}
/**
 * @brief 
 * 
 * @param p_imsi 
 * @param size 
 * @return true 
 * @return false 
 */
bool get_imsi(dev_msg_t *dev_msg, uint32_t size)
{
    bool ret;
#if DEV_IMEI
    ret = read_imsi(dev_msg->msg,size);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    strcpy(dev_msg->msg,"imsi_test\0");
    dev_msg->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
    ret = true;
#endif //DEV_IMEI
    return ret;
}
/**
 * @brief 
 * 
 * @param cmd_type 
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_dev_msg_type_t cmd_type,test_inter_type chn)
{
    bool ret = 0;
    dev_msg_t dev_msg;
    memset(&dev_msg,0,sizeof(dev_msg));
    switch(cmd_type)
    {
        case PROTOCOL_DEV_MSG_TYPE_SN:
            DBG_LOG("read device message of SN\r\n");
            ret = get_sn(&dev_msg,sizeof(dev_msg.msg));
            
            break;
        case PROTOCOL_DEV_MSG_TYPE_MAC:
            DBG_LOG("read device message of MAC\r\n");
            ret = get_mac(dev_msg.msg,sizeof(dev_msg.msg));
           
            break;
        case PROTOCOL_DEV_MSG_TYPE_IMEI:
            DBG_LOG("read device message of IMEI\r\n");
            ret = get_imei(&dev_msg,sizeof(dev_msg.msg));
            
            break;
        case PROTOCOL_DEV_MSG_TYPE_IMSI:
            DBG_LOG("read device message of IMSI\r\n");
            ret = get_imsi(&dev_msg,sizeof(dev_msg.msg));
            
            break;
        default: 
            ret = false;
            DBG_LOG("the cmd of get device message is invalid\r\n");
            break;
    }
    dev_msg.type = cmd_type;
    dev_msg.st = (ret == true)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
    response(&dev_msg,chn);      
    return ret;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool dev_msg_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_dev_msg_req_t cmd;
    cmd_decode(&cmd,p_cmd,cmd_length);
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    //protocol_dev_msg_rsp_t msg_rsp;
    ret = cmd_handler(cmd.Cmd,chn);
    return ret;
}

#if DEV_MSG_VERIFY
static void verify_req(void)
{
    protocol_dev_msg_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    cmd.Cmd = PROTOCOL_CMD_TYPE_MSG_SN;
    //cmd.Cmd = PROTOCOL_CMD_TYPE_MSG_MAC;
    //cmd.Cmc = PROTOCOL_CMD_TYPE_MSG_IMEI;
    //cmd.Cmd = PROTOCOL_CMD_TYPE_MSG_IMSI;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_dev_msg_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_READ_DEV_MSG_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{
    verify_req();
}
#endif  //DEV_MSG_VERIFY