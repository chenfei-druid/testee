#ifndef TEST_UART_H
#define TEST_UART_H

#include <stdint.h>
//#include <stdbool.h>

#define UART_RX_PIN     9
#define UART_TX_PIN     8

#define UART_RX_BUF     128
#define UART_TX_BUF     128

#define UART_RX_TIMEOUT 50     //mS

void test_uart_init(void);
void test_close_uart(void);
uint32_t test_uart_trans(uint8_t * buf, uint8_t length);
uint32_t test_uart_receive(void * const buf, uint8_t max_size);
uint32_t uart_pkg_send(void * const buffer, uint16_t const length);
int32_t uart_pkg_receive(void const * const buf, uint16_t length, uint32_t timeout);

#endif //TEST_UART_H
