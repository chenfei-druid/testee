
/**
 * @brief 
 * 
 * @file testee_gps.h
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_GPS_H
#define TEST_GPS_H

#include <stdint.h>
#include "test_upload.h"
#include "testee_gps.pb.h"

#define GPS_VERIFY 0

typedef struct
{
    int32_t Latitude ; // 纬度: 度, 精确到小数点后第7位, 定位失败为200
    int32_t Longitude ; // 经度: 度, 精确到小数点后第7位,
    int32_t  Quality ; // 定位质量: 0 - 无定位, 1 - 普通定位, 2 - 差分定位, 6 - 粗略定位
    int32_t  CNR ;
}gps_msg_t;
typedef struct
{
    protocol_status_type_t st;
    protocol_gps_cmd_type_t cmd;
}gps_res_t;
typedef struct
{
    gps_res_t res;
    gps_msg_t msg;
}gps_rsp_t;

#if GPS_VERIFY
void pb_verify(void);
#endif //LORA_VERIFY

bool gps_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_GPS_H
