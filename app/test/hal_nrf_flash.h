/**--------------------------------------------------------------------------------------------------------
** Commpany     :       Druid
** Created by   :		chenfei
** Created date :		2017-12-1
** Version      :	    0.0
** Descriptions :		user_nrf_flash_fds.h
*/

#ifndef _USER_NRF_FLASH_H_
#define _USER_NRF_FLASH_H_


#include <stdint.h>
#include <stdbool.h>


#define     USER_PAGE_SIZE                   4096                //1K word of one page size
#define     USER_MEMORY_START           0x00060000 
#define     USER_MEMORY_END             0x0006A000
#define     USER_PAGE_MAX               ((USER_MEMORY_END - USER_MEMORY_START) / USER_PAGE_SIZE)


void user_nrf_flash_init(void);
bool user_nrf_flash_word_write(uint32_t * p_address, uint32_t value);
bool user_nrf_flash_page_erase(uint32_t *page_start,int8_t page_num);
bool user_nrf_flash_words_write(uint32_t * p_address, const uint32_t *const word_buff, const uint32_t len);
bool user_nrf_flash_word_read(uint32_t * p_address, uint32_t * value);
bool user_nrf_flash_read_words(uint32_t * p_address, uint32_t * data_buff, uint32_t len);

#endif //_USER_NRF_FLASH_H_

