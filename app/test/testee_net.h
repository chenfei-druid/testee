
/**
 * @brief 
 * 
 * @file testee_net.h
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_NET_H
#define TEST_NET_H

#include <stdint.h>
#include "test_upload.h"
#include "testee_net.pb.h"

#define NET_VERIFY 0

typedef struct
{
    uint32_t dt;
    protocol_status_type_t st;
    protocol_net_cmd_type_t cmd;

}net_rsp_t;

#if NET_VERIFY
void pb_verify(void);
#endif //NET_VERIFY

bool net_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_NET_H
