
/**
 * @brief 
 * 
 * @file testee_sn.c
 * @date 2018-08-13
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_sn.pb.h"
#include "testee_sn.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "simple.h"
#include "test_define.pb.h"
#include "user_data_pkg.h"
#include "hal_nrf_flash.h"
#include "hal_cfg.h"
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_sn_req_t *p_pro_req,
                        char * const p_sn,
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    p_pro_req->SN.funcs.decode            = &user_app_decode_repeated_var_string;
    p_pro_req->SN.arg                     = p_sn;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_sn_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command write sn decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command write sn decode failed\r\n");
        return false;
    }  
}
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 * @param type 
 * @return true 
 * @return false 
 */
static bool save_sn(char *const p_sn)
{
    if(p_sn == NULL)
    {
        DBG_LOG("test write sn pointer is null");
        return false;
    }
    if(strlen(p_sn) > (sizeof(sn_record_t)-4))
    {
        DBG_LOG("SN length is large");
        return false;
    }
    bool ret = false;
#if DEV_SN
    ret = write_sn(dev_msg->msg,size);
#else   
    sn_record_t sn;
    memset(&sn,0,sizeof(sn));

    sn.len = strlen(p_sn);
    memcpy(sn.sn,p_sn,sn.len);
    ret = user_nrf_flash_page_erase((uint32_t *)USER_MEMORY_START,1);
    if(ret)
    {
        ret = user_nrf_flash_words_write((uint32_t *)USER_MEMORY_START,(uint32_t *)&sn,sn.len);
    }
    //ret = Record_Write(RECORD_TYPE_SN,(uint8_t *)&sn);  
#endif // DEV_SN
    DBG_LOG("write sn %s %s" ,(char *)p_sn,ret?"successed":"failed");
    return ret;
}
/**
 * @brief 
 * 
 * @param cmd_type 
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(char * const sn,test_inter_type chn)
{
    bool ret = 0;
    protocol_status_type_t status;

    ret = save_sn(sn);
    
    status = (ret != false)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;

    simple_response(status,PROTOCOL_MSG_TYPE_SN_RSP,chn);
       
    return ret; ;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool write_sn_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_sn_req_t cmd;
    char sn[20];
    memset(sn,0,sizeof(sn));
    if(cmd_decode(&cmd,sn,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    ret = cmd_handler(sn,chn);
    return ret;
}

#if SN_VERIFY
static void verify_req(void)
{
    protocol_sn_req_t cmd;
    char sn[] = "123456789abc\0";
    uint8_t req_buf[50] = {0};
    pb_ostream_t            m_stream ;  

    memset(req_buf,0,sizeof(req_buf));

    app_get_indetity_msg(&cmd.Head);

    cmd.has_Head = 1;
    if(cmd.has_Head)
    {
        char id[15];
        memset(id,0,sizeof(id));
        app_iden_get_device_id_char(id);

        cmd.Head.MsgIndex = 13;

        cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
        cmd.Head.DeviceID.arg          = id; 
    }
    cmd.SN.funcs.encode     = &user_app_encode_repeated_var_string;
    cmd.SN.arg              = sn;

    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_sn_req_fields,&cmd);
    
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_SN_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{
    verify_req();
}
#endif