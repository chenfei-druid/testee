
/**
 * @brief 
 * 
 * @file test_dev_data.c
 * @date 2018-08-01
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_dev_data.h"
#include "user_app_identity.h"
#include "user_data_pkg.h"
#include "test_define.pb.h"
#include "HexStr.h"
#include "hal_cfg.h"
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_dev_data_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_dev_data_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command device data decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command device data decode failed\r\n");
        return false;
    }  
}
#if DEV_DT_VERIFY
/**
 * @brief 
 * 
 * @param buf 
 * @param len 
 */
static void verify_rsp_encode(uint8_t * const buf, uint32_t len)
{
    
    DBG_LOG("len = %d ",len);
    for(uint8_t i=0;i<len;i++)
    {
        DBG_B("%2x ",buf[i]);
    }
    DBG_LOG("\r\n");
    /*
    uint8_t xbuf[]={0x08,0x00,0x10,0x00,0x1A,0x16,0x18,0xD8,0xB3,0xD1,0xAD,0x0D,0x20,0x0D, \
                   0x2A,0x0C,0x63,0x61,0x30,0x33,0x31,0x63,0x37,0x61,0x32,0x34,0x30,0x38 \
                   };
    len = sizeof(xbuf);
    memcpy(buf,xbuf,len);
    */
    
    protocol_dev_data_rsp_t rsp;
    char dev_id[15];  
    
    memset(dev_id,0,sizeof(dev_id));

    rsp.Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    rsp.Head.DeviceID.arg          = dev_id;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_dev_data_rsp_fields,&rsp);
    if(res)
    {

    }
}
#endif //DEV_DT_VERIFY
/**
 * @brief 
 * 
 * @param ret 
 * @param chn 
 * @return uint32_t 
 */
static uint32_t rsp_encode(uint8_t *rsp_buf,
                            uint32_t max_size,
                          dev_data_t *dev_dt)
{
    protocol_dev_data_rsp_t rsp;
    
    char dev_id[15] ="12345abcd\0"; //for tes

    memset(dev_id,0,sizeof(dev_id));
    memset(rsp_buf,0,sizeof(rsp_buf));

    app_get_indetity_msg(&rsp.Head);
    app_iden_get_device_id_char(dev_id);

    rsp.has_Head = 1;
    rsp.Head.MsgIndex = 13;

    rsp.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.Head.DeviceID.arg          = dev_id;

    //rsp.DevId.funcs.encode = &user_app_encode_repeated_var_string;
    //rsp.DevId.arg          = dev_id;

    rsp.Cmd = dev_dt->type;
    rsp.Data = dev_dt->dt;
    rsp.Status             = dev_dt->st;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(rsp_buf,max_size);
    bool res = pb_encode(&m_stream,protocol_dev_data_rsp_fields,&rsp);
    if(res)
    {
        DBG_LOG("device data response encode successed\r\n");
    #if DEV_DT_VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("device data response encode failed\r\n");
        return 0;
    }
}
static uint32_t response(dev_data_t* dt,
                         test_inter_type chn)
{
    unsigned char rsp_buf[200];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    DBG_LOG("response device data = %d\r\n",dt->dt);
    len = rsp_encode(rsp_buf,sizeof(rsp_buf),dt);
    if(len <= 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[65];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_READ_DEV_DATA_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("device data response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("device data response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool voltage(dev_data_t *read_dt)
{
#if DEV_VOLTAGE
    bool ret;
    ret = read_voltage(uint32_t *p_dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
   read_dt->dt = 3956;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_VOLTAGE
    return true;
}

/**
 * @brief get temperature data
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool temp(dev_data_t *read_dt)
{
#if DEV_TEMP
    bool ret;
    ret = read_temp(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 254;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_TEMP
    return true;
}

/**
 * @brief get humidity data
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool humi(dev_data_t *read_dt)
{
#if DEV_HUMI
    bool ret;
    ret = read_humi(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 82;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_HUMI
    return true;
}
/**
 * @brief get lux data
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool lux(dev_data_t *read_dt)
{
#if DEV_LUX
    bool ret;
    ret = read_humi(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 199;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_LUX
    return true;
}

/**
 * @brief get water sensor data
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool water(dev_data_t *read_dt)
{
#if DEV_WATER
    bool ret;
    ret = read_humi(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 23;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_WATER
    return true;
}

/**
 * @brief get press data
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool press(dev_data_t *read_dt)
{
#if DEV_PRESS
    bool ret;
    ret = read_humi(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 66;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_PRESS
    return true;
}

/**
 * @brief get accelerate test result
 * 
 * @param p_dt 
 * @return true 
 * @return false 
 */
static bool acc(dev_data_t *read_dt)
{
#if DEV_ACC
    bool ret;
    ret = read_humi(&read_dt->dt);
    read_dt.st = (ret == 1)?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
#else
    read_dt->dt = 1234;
    read_dt->st = PROTOCOL_STATUS_TYPE_SUCCESSED;
    //read_dt->st = PROTOCOL_STATUS_TYPE_WITHOUT_FEATUER;
#endif //DEV_ACC
    return true;
}

/**
 * @brief 
 * 
 * @param cmd_type 
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_dev_data_type_t cmd_type,test_inter_type chn)
{
    bool ret = 0;
    //uint32_t dt = 0;
    dev_data_t read_dt;
    switch(cmd_type)
    {
        case PROTOCOL_DEV_DATA_TYPE_VOLTAGE:
            DBG_LOG("read device message of voltage\r\n");
            ret = voltage(&read_dt);
            break;
        case PROTOCOL_DEV_DATA_TYPE_TEMP:
            DBG_LOG("read device message of temperature\r\n");
            ret = temp(&read_dt);          
            break;
        case PROTOCOL_DEV_DATA_TYPE_HUMI:
            DBG_LOG("read device message of humidity\r\n");
            ret = humi(&read_dt); 
            break;
        case PROTOCOL_DEV_DATA_TYPE_LUX:
            DBG_LOG("read device message of lux\r\n");
            ret = lux(&read_dt); 
            break;
        case PROTOCOL_DEV_DATA_TYPE_WATER:
            DBG_LOG("read device message of water sendor\r\n");
            ret = water(&read_dt); 
            break;
        case PROTOCOL_DEV_DATA_TYPE_PRESS:
            DBG_LOG("read device message of pressure\r\n");
            ret = press(&read_dt); 
            break;
        case PROTOCOL_DEV_DATA_TYPE_ACC:
            DBG_LOG("read device message of accelerate\r\n");
            ret = acc(&read_dt); 
            break;
        default: 
            ret = false;
            DBG_LOG("the cmd of get device message is invalid\r\n");
            break;
    }
    read_dt.type = cmd_type;
    response(&read_dt,chn);   
    return ret ;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool dev_data_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_dev_data_req_t cmd;
    cmd_decode(&cmd,p_cmd,cmd_length);
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    ret = cmd_handler(cmd.Cmd,chn);
    return ret;
}
#if DEV_DT_VERIFY
static void verify_req(void)
{
    protocol_dev_data_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    //cmd.Cmd = PROTOCOL_DEV_DATA_TYPE_VOLTAGE;
    cmd.Cmd = PROTOCOL_DEV_DATA_TYPE_TEMP;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_dev_data_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_READ_DEV_DATA_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{
    verify_req();
}
#endif  //DEV_DT_VERIFY

