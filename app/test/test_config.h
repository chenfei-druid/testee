#ifndef TEST_CONFIG_H
#define TEST_CONFIG_H

/*UART config*/
#define UART_RX_PIN     9
#define UART_TX_PIN     8

#define UART_RX_BUF     128
#define UART_TX_BUF     128

#define UART_RX_TIMEOUT 50     //mS

/*LED config*/
#define RUN_INDEX   23
//#define CMD_INDEX   23

/*GPS config*/

/*NET config*/

/*LORA config*/


#endif //TEST_CONFIG_H
