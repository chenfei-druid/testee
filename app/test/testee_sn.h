/**
 * @brief 
 * 
 * @file testee_sn.h
 * @date 2018-08-13
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TEST_SN_H
#define TEST_SN_H

#include "test_upload.h"

#define SN_VERIFY 0

typedef struct
{
    //uint32_t timestamp;
    uint8_t sn[12];
    uint32_t len;
}sn_record_t;

#if SN_VERIFY
void pb_verify(void);
#endif //SN_VERIFY

bool write_sn_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TEST_SN_H
