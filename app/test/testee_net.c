

/**
 * @brief 
 * 
 * @file testee_net.c
 * @date 2018-08-15
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "app_freertos.h"
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "user_app_log.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "testee_net.pb.h"
#include "testee_net.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "record.h"
#include "test_define.pb.h"
#include "simple.h"
#include "user_data_pkg.h"

static bool start(void)
{
    bool ret;
#if NET_ENABLE
    ret = net_start();
#else
    ret = true;
#endif //
    return ret;
}
static bool loop_back(uint32_t *p_out,uint32_t dt)
{
    bool ret;
#if NET_ENABLE
    ret = net_loop_back(p_out,dt);
#else
    *p_out = ~dt;
    ret = true;
#endif //
    return ret;
}
/**
 * @brief 
 * 
 * @param p_pro_req 
 * @param p_buf 
 * @param len 
 * @return true 
 * @return false 
 */
static bool cmd_decode(protocol_net_req_t *p_pro_req, 
                        uint8_t const * const p_buf,
                        uint32_t const len)
{
    bool ret;
    pb_istream_t i_stream ;

    char id[15];
    
    memset(id,0,sizeof(id));

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    ret = pb_decode(&i_stream,protocol_net_req_fields,p_pro_req); 
    DBG_LOG("command net test decode %s\r\n",ret?"successed":"failed");
    return ret;
}
static uint32_t rsp_encode(uint8_t *rsp_buf,
                           uint32_t max_size,
                           protocol_net_rsp_t *rsp)
{
    memset(rsp_buf,0,sizeof(rsp_buf));

    rsp->has_Head = 0;
    if(rsp->has_Head)
    {
        char dev_id[15] ="12345abcd\0"; //for tes
        memset(dev_id,0,sizeof(dev_id));
        app_get_indetity_msg(&rsp->Head);
        app_iden_get_device_id_char(dev_id);

        rsp->Head.MsgIndex = 13;

        rsp->Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
        rsp->Head.DeviceID.arg          = dev_id;
    }

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(rsp_buf,max_size);
    bool res = pb_encode(&m_stream,protocol_net_rsp_fields,&rsp);
    if(res)
    {
        DBG_LOG("net test response encode successed\r\n");
    #if DEV_DT_VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("net test response encode failed\r\n");
        return 0;
    }
}

static uint32_t response(protocol_net_rsp_t *ret_dt,
                         test_inter_type chn)
{
    unsigned char rsp_buf[50];
    int32_t len = 0;
    memset(rsp_buf,0,sizeof(rsp_buf));
    len = rsp_encode(rsp_buf,sizeof(rsp_buf),ret_dt);
    if(len <= 0)
    {
        return false;
    }
    pkg_prot_frame_t p_prot_frame_t;
    uint8_t send_buf[65];
    len = user_add_head_to_protbuf(&p_prot_frame_t,rsp_buf,len,PROTOCOL_MSG_TYPE_NET_RSP);
    DBG_LOG("package len = %d\r\n",len);
    if(len > sizeof(send_buf))
    {
        return 0;
    }
    memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
    memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
    DBG_LOG("net test response, len = %d\r\n",len);
    len = test_upload_data(send_buf,len,chn);
    DBG_LOG("net test response over!\r\n",len);
    return len?true:false;
}
/**
 * @brief 
 * 
 * @param cmd_type  
 * @param chn 
 * @return true 
 * @return false 
 */
static bool cmd_handler(protocol_net_req_t *cmd,test_inter_type chn)
{
    
    //protocol_status_type_t status;
    protocol_net_cmd_type_t cmd_type = cmd->Cmd;
    bool ret;
    //uint32_t ret_rand;
    //net_rsp_t ret_dt;
    protocol_net_rsp_t net_rsp;
    switch(cmd_type)
    {
        case PROTOCOL_NET_CMD_TYPE_START:
            DBG_LOG("net test: start net module\r\n");
            ret = start();
            net_rsp.Cmd = PROTOCOL_NET_CMD_TYPE_START;
            //status = ret?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;
            //response(status,chn);
            break;
        case PROTOCOL_NET_CMD_TYPE_LOOP_BACK_MODE:
            DBG_LOG("net test: loop back test\r\n");
            if(cmd->has_Random)
            {
                net_rsp.has_Data = 1;
                ret = loop_back(&net_rsp.Data,cmd->Random);
                ret = true;
            }
            else
            {
                net_rsp.has_Data = 0;
                net_rsp.Data = 0xffff;
                ret = false;
            }
            net_rsp.Cmd = PROTOCOL_NET_CMD_TYPE_LOOP_BACK_MODE;
            //ret_rand = (cmd->has_Random)?(~cmd->Random):0xffff;
            break;
        default:
            DBG_LOG("net test: invalid operate type\r\n");
            break;
    }
    net_rsp.Status = ret?PROTOCOL_STATUS_TYPE_SUCCESSED:PROTOCOL_STATUS_TYPE_FAILED;

    response(&net_rsp,chn);
    return ret;
}
/**
 * @brief 
 * 
 * @param p_cmd 
 * @param cmd_length 
 * @param chn 
 * @return true 
 * @return false 
 */
bool net_test_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_net_req_t cmd;
    if(cmd_decode(&cmd,p_cmd,cmd_length) != true)
    {
        return false;
    }
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }  
    ret = cmd_handler(&cmd,chn);
    return ret;
}

#if NET_VERIFY
static void verify_req(void)
{
    protocol_net_req_t cmd;
    uint8_t req_buf[50] = {0};
    char id[15];

    memset(req_buf,0,sizeof(req_buf));
    memset(id,0,sizeof(id));

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;
    
    cmd.Cmd = PROTOCOL_NET_CMD_TYPE_START;
    //cmd.Cmd = PROTOCOL_NET_CMD_TYPE_LOOP_BACK_MODE;

    cmd.has_Random = 1;
    cmd.Random     = 0xaa;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_net_req_fields,&cmd);
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_NET_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);

        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        test_upload_data(str,len*2,TEST_INTER_UART);
    }
}
void pb_verify(void)
{

    verify_req();

}
#endif  //CHN_VERIFY
