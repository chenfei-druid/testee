
#ifndef TESTER_SELFTEST_H
#define TESTER_SELFTEST_H

#include <stdint.h>
#include "test_upload.h"

#define SELFTEST_VERIFY 0

bool tester_self_test(unsigned char *p_cmd,uint32_t cmd_length,test_inter_type chn);

#if SELFTEST_VERIFY
void pb_verify(void);
#endif
#endif //TESTER_SELFTEST_H
