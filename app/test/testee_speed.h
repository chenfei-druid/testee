/**
 * @brief 
 * 
 * @file testee_speed.h
 * @date 2018-08-14
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#ifndef TESTEE_SPEED_H
#define TESTEE_SPEED_H

#include <stdint.h>
#include "test_upload.h"

#define SPEED_VERIFY 0


#if SPEED_VERIFY
void pb_verify(void);
#endif //SPEED_VERIFY

bool set_speed_handler(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn);

#endif  //TESTEE_SPEED_H
