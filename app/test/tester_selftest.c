/**
 * @brief 
 * 
 * @file tester_selftest.c
 * @date 2018-07-31
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.  All Rights Reserved.
 */
#include <stdint.h>
#include "pb_common.h"
#include "pb_decode.h"
#include "pb_encode.h"
#include "tester_self.pb.h"
#include "user_app_log.h"
//#include "test_upload.h"
#include "ble_app.h"
#include "test_pb_callback.h"
#include "tester_selftest.h"
#include "simple.h"
#include "HexStr.h"
#include "user_app_identity.h"
#include "test_parallel.h"
#include "test_upload.h"
#include "test_define.pb.h"
#include "user_data_pkg.h"
/*
static bool encode_struct(pb_ostream_t *stream, const pb_field_t fields[],  void * const *src_struct)
{   
    bool res;
    protocol_self_test_rsp_t *rsp_t;
    rsp_t = (protocol_self_test_rsp_t *)*src_struct;
    if (!pb_encode_tag_for_field(stream, fields))
    {
        DBG_LOG("tester self-test encode tag failed\r\n\0");
        return false;
    }
    res = pb_encode_submessage(stream, protocol_self_test_rsp_fields, rsp_t); 
    DBG_LOG("tester self-test encode struct is %s\r\n", res? "successed":"failed");
    return res;
}
*/
static bool decode(protocol_self_test_req_t *p_pro_req, 
                    uint8_t const * const p_buf,
                    uint32_t const len)
{
    bool res;
    pb_istream_t i_stream ;
    char id[15];

    p_pro_req->Head.DeviceID.funcs.decode = &user_app_decode_repeated_var_string;
    p_pro_req->Head.DeviceID.arg = id;

    i_stream = pb_istream_from_buffer(p_buf,len);
    res = pb_decode(&i_stream,protocol_self_test_req_fields,p_pro_req);
    if(res == true)
    {
        DBG_LOG("command decode successed\r\n");

        return true;
    }
    else
    {
        DBG_LOG("command decode failed\r\n");
        return false;
    }  
}
#if SELFTEST_VERIFY
/*
static void verify_rsp_encode(unsigned char * const buf, uint32_t len)
{
    protocol_self_test_rsp_t rsp_verify;
    char dev_id[20];
    memset(dev_id,0,sizeof(dev_id));

    rsp_verify.DevId.funcs.decode = &user_app_decode_repeated_var_string;
    rsp_verify.DevId.arg = dev_id;

    pb_istream_t i_stream ;
    i_stream = pb_istream_from_buffer(buf,len);
    bool res = pb_decode(&i_stream,protocol_self_test_rsp_fields,&rsp_verify);
}
*/
#endif
/*
static void response(bool ret,test_inter_type chn)
{
    protocol_self_test_rsp_t rsp;
    unsigned char rsp_buf[20];
    //char dev_id[15] ="12345abcd\0"; //for test
    char dev_id[15] ="\0";
    protocol_status_type_t status;
    if(ret)
    {
       status = PROTOCOL_STATUS_TYPE_SUCCESSED;
    }
    else
    {
        status = PROTOCOL_STATUS_TYPE_FAILED;
    }
    memset(rsp_buf,0,sizeof(rsp_buf));
    //memset(dev_id,0,sizeof(dev_id));
    rsp.DevId.funcs.encode = &user_app_encode_repeated_var_string;
    rsp.DevId.arg          = dev_id;

    rsp.Status             = status;

    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(rsp_buf,sizeof(rsp_buf));
    bool res = pb_encode(&m_stream,protocol_self_test_rsp_fields,&rsp);
    if(res)
    {
        DBG_LOG("selftest response encode successed\r\n");
    #if VERIFY
        verify_rsp_encode(rsp_buf,m_stream.bytes_written);
    #endif  // VERIFY    
        test_upload_data(rsp_buf,m_stream.bytes_written,chn);
        return m_stream.bytes_written;        
    }
    else
    {
        DBG_LOG("selftest response encode failed\r\n");
        return 0;
    }
}
*/

bool tester_self_test(uint8_t *p_cmd,uint32_t cmd_length,test_inter_type chn)
{
    bool ret = false;
    protocol_self_test_req_t cmd;
    decode(&cmd,p_cmd,cmd_length);
    if(cmd.has_Head)
    {
        //time = cmd.Head.Timestamp;
    }
    search_target_t target;
    memset(&target,0,sizeof(search_target_t));
    target.mode = NEED_NONE;

    //search any target device
    //if(ble_scan_slave(10*1000) != true)
    if(ble_search_slave(&target,10*1000) != true)
    {
        //test_answer_err(chn);
        DBG_LOG("self test failed");
        ret = false;
    }
    else
    {
        //test_answer_ok(chn);
        DBG_LOG("self test success");
        ret = true;
    }  
    simple_response(ret,PROTOCOL_MSG_TYPE_SELF_TEST_RSP,chn);
    return ret;
}
#if SELFTEST_VERIFY
static void verify_req(void)
{
    protocol_self_test_req_t cmd;
   char id[15];

    app_get_indetity_msg(&cmd.Head);
    app_iden_get_device_id_char(id);

    cmd.has_Head = 1;
    cmd.Head.MsgIndex = 13;

    cmd.Head.DeviceID.funcs.encode = &user_app_encode_repeated_var_string;
    cmd.Head.DeviceID.arg          = id;

    uint8_t req_buf[50] = {0};
    pb_ostream_t            m_stream ;    
    m_stream = pb_ostream_from_buffer(req_buf,sizeof(req_buf));
    bool res = pb_encode(&m_stream,protocol_self_test_req_fields,&cmd);
    
    if(res)
    {
        uint8_t send_buf[60];

        pkg_prot_frame_t p_prot_frame_t;
        int32_t len = 0;
    
        len = user_add_head_to_protbuf(&p_prot_frame_t,req_buf,m_stream.bytes_written,PROTOCOL_MSG_TYPE_SELF_TEST_REQ);
        if(len > sizeof(send_buf))
        {
            return;
        }
        memcpy(send_buf,(uint8_t *)&p_prot_frame_t.prot_head,sizeof(p_prot_frame_t.prot_head));
        memcpy(send_buf+sizeof(p_prot_frame_t.prot_head),p_prot_frame_t.p_prot_data,p_prot_frame_t.prot_head.frame_len);
        //len = test_upload_data(send_buf,len,chn);
        //return len;
    
        char str[200];
        memset(str,0,sizeof(str));
        HexToStr(str,send_buf,len);
        print_string("\r\n\r\nThe output data\r\n\0");
        print_string(str);
        print_string("\r\n\0");
        //tester_self_test(req_buf,m_stream.bytes_written,TEST_INTER_UART);
    }
}
void pb_verify(void)
{
    //test_inter_type chn = TEST_INTER_UART;

    verify_req();
    //bool ret = false;
    //response(ret,chn);
}
#endif