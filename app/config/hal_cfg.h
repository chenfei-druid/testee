/*!
 *    @file  hal_cfg.h
 *   @brief  The config file for hal layer
 *
 *  @author  Dale.J (dj), Dale.J@zoho.com
 *
 *  @internal
 *       Created:  05/17/2018
 *      Revision:  none
 *  Organization:  Druid Tech
 *     Copyright:  Copyright (c) 2016, Dale.J
 *
 *  This source code is released for free distribution under the terms of the
 *  GNU General Public License as published by the Free Software Foundation.
 */

#ifndef __HAL_CFG_H__
#define __HAL_CFG_H__

/* Defines -------------------------------------------------------------------*/
#define SW_VERSION        1

// FLASH
#define HAL_CFG_FLS_NSS   30
#define HAL_CFG_FLS_MISO  29
#define HAL_CFG_FLS_MOSI  28
#define HAL_CFG_FLS_SCK   27

// KX022
#define HAL_CFG_ACC_INT   12
#define HAL_CFG_ACC_NSS   11
#define HAL_CFG_ACC_SCK   10
#define HAL_CFG_ACC_MISO  9
#define HAL_CFG_ACC_MOSI  8

// I2C
#define HAL_CFG_I2C_SCL   26
#define HAL_CFG_I2C_SDA   25

// HALL
#define HAL_CFG_HALL_IRQ  20

// GPS
#define HAL_CFG_GPS_PWR    31
#define HAL_CFG_GPS_BKP    5
#define HAL_CFG_GPS_RXD    6
#define HAL_CFG_GPS_TXD    7

// BATTERY
#define HAL_CFG_BATTERY    4

// LED
#define HAL_CFG_LED_GREEN  21

// CELL
#define HAL_CFG_CELL_PWR   13
#define HAL_CFG_CELL_RI    14
#define HAL_CFG_CELL_DTR   15
#define HAL_CFG_CELL_TXD   16
#define HAL_CFG_CELL_RXD   17
#define HAL_CFG_CELL_STA   18
#define HAL_CFG_CELL_KEY   19

//read device data, if without feature, set to 0
#define DEV_VOLTAGE 0
#define DEV_TEMP    0
#define DEV_HUMI    0
#define DEV_LUX     0
#define DEV_WATER   0
#define DEV_PRESS   0
#define DEV_ACC     0

#define DEV_IMEI    0
#define DEV_IMSI    0
#define DEV_SN      0

#define PWR_SET     0
#define LORA_ENABLE 0

//read device battary voltage, unit is mV
#if DEV_VOLTAGE
bool read_voltage(uint32_t *p_dt);
#endif //DEV_VOLTAGE

//read device temperature,unit is 0.1 C
#if DEV_TEMP
bool read_temp(uint32_t *p_dt);
#endif  //DEV_TEMP

//read sensor data of humidity
#if DEV_HUMI
bool read_humi(uint32_t *p_dt);
#endif  //DEV_HUMI

//read sensor data of lux
#if DEV_LUX
bool read_lux(uint32_t *p_dt);
#endif  //DEV_LUX

//read sensor data of water
#if DEV_WATER
bool read_water(uint32_t *p_dt);
#endif //DEV_WATER

//read sensor data of press
#if DEV_PRESS
bool read_press(uint32_t *p_dt);
#endif //DEV_PRESS

//read sensor data of accelerate, the result of selftest
#if DEV_ACC
bool read_acc(uint32_t *p_dt);
#endif //DEV_ACC

#if DEV_IMEI
bool read_imei(char * msg,uint8_t size);
#endif //DEV_IMEI

#if DEV_IMEI
bool read_imsi(char * msg,uint8_t size);
#endif //DEV_IMEI

#if DEV_SN
bool read_sn(char * msg,uint8_t size);
bool write_sn(char *msg);
#endif //DEV_SN

#if PWR_SET
bool set_power_min(void);
bool set_power_max(void);
#endif //PWR_SET

#if LORA_ENABLE
bool lora_power_on(void);
bool lora_frq_set(uint32_t);
bool lora_pwr_set(uint32_t);
bool lora_speed_set(uint32_t);
bool lora_sf_set(uint32_t);
bool lora_bw_set(uint32_t);
bool lora_cr_set(uint32_t);
bool lora_sleep();
bool lora_loop_back(uint32_t);
#endif //LORA_ENABLE

#if NET_ENABLE
bool net_start(void);
bool net_loop_back(uint32_t *out_dt,uint32_t give_dt);
#endif //NET_ENABLE
#endif // #ifndef __HAL_CFG_H__
