/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.6-dev at Mon Aug 20 19:48:29 2018. */

#include "testee_devmsg.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t protocol_dev_msg_req_fields[3] = {
    PB_FIELD(  1, MESSAGE , OPTIONAL, STATIC  , FIRST, protocol_dev_msg_req_t, Head, Head, &protocol_identity_msg_fields),
    PB_FIELD(  2, UENUM   , REQUIRED, STATIC  , OTHER, protocol_dev_msg_req_t, Cmd, Head, 0),
    PB_LAST_FIELD
};

const pb_field_t protocol_dev_msg_rsp_fields[5] = {
    PB_FIELD(  1, UENUM   , REQUIRED, STATIC  , FIRST, protocol_dev_msg_rsp_t, Status, Status, 0),
    PB_FIELD(  2, UENUM   , REQUIRED, STATIC  , OTHER, protocol_dev_msg_rsp_t, Cmd, Status, 0),
    PB_FIELD(  3, STRING  , REQUIRED, CALLBACK, OTHER, protocol_dev_msg_rsp_t, Msg, Cmd, 0),
    PB_FIELD(  4, MESSAGE , OPTIONAL, STATIC  , OTHER, protocol_dev_msg_rsp_t, Head, Msg, &protocol_identity_msg_fields),
    PB_LAST_FIELD
};


/* Check that field information fits in pb_field_t */
#if !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_32BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 * 
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in 8 or 16 bit
 * field descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(protocol_dev_msg_req_t, Head) < 65536 && pb_membersize(protocol_dev_msg_rsp_t, Head) < 65536), YOU_MUST_DEFINE_PB_FIELD_32BIT_FOR_MESSAGES_protocol_dev_msg_req_protocol_dev_msg_rsp)
#endif

#if !defined(PB_FIELD_16BIT) && !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_16BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 * 
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in the default
 * 8 bit descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(protocol_dev_msg_req_t, Head) < 256 && pb_membersize(protocol_dev_msg_rsp_t, Head) < 256), YOU_MUST_DEFINE_PB_FIELD_16BIT_FOR_MESSAGES_protocol_dev_msg_req_protocol_dev_msg_rsp)
#endif


/* @@protoc_insertion_point(eof) */
