/**
 * @brief 
 * 
 * @file ble_druid_svc.h
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-10
 */
#ifndef BLE_DRUID_SVC_H
#define BLE_DRUID_SVC_H

#include <stdint.h>
#include <stdbool.h>

#define BLE_MAX_LEN         20
#define BLE_RX_BUF_SIZE     800
void ble_init(bool *erase_bonds);
uint32_t ble_act_central_send(void * const p_data, uint16_t length);
uint32_t ble_act_slave_send(void * const p_data, uint16_t length);
bool ble_connect_slave(uint16_t * conn, uint8_t *p_mac,uint32_t timeout);
bool ble_disconnect_slave(uint16_t conn);
bool ble_scan_slave(uint32_t timeout);
uint8_t get_device_mac_addr(uint8_t * const p_mac, uint8_t const size);

#endif //BLE_DRUID_SVC_H
