#ifndef BLE_TRANS_H
#define BLE_TRANS_H

#include "app_freertos.h"
#include "stdint.h"

#define     BLE_TX_COMPLETE_WAIT    4000     //MS
#define     BLE_S_MAX_LEN           20

QueueHandle_t ble_c_create_queue(void);
QueueHandle_t ble_s_create_queue(void);
SemaphoreHandle_t ble_c_create_sema(void);
SemaphoreHandle_t ble_s_create_sema(void);
int32_t ble_c_receive(void * const buffer, uint16_t max_size, uint32_t timeout);
int32_t ble_s_receive(void * const buffer, uint16_t max_size, uint32_t timeout);
int32_t ble_c_send(void * const p_data, uint16_t length);
int32_t ble_s_send(void * const p_buf, uint16_t const length);
int32_t ble_s_pkg_send(void * const buffer, uint16_t length);
int32_t ble_c_pkg_send(void * const buffer, uint16_t length);
#endif //BLE_TRANS_H
