/**
 * @brief 
 * 
 * @file ble_druid_svc.h
 * @author Chenfei
 * @copyright (c) 2016-2018 by Druid Technology Co., Ltd.
 * All Rights Reserved.
 * @date 2018-07-10
 */
#ifndef BLE_DRUID_SVC_H
#define BLE_DRUID_SVC_H

#include <stdint.h>
#include <stdbool.h>

#define BLE_MAX_LEN         20
#define BLE_RX_BUF_SIZE     800
#define MAC_LEN 6

typedef enum
{
    INVALID_MODE = 0,
    NEED_NAME_AND_ADDR, //attention name and addr, when search slave device
    NEED_ONLY_NAME,         //only attention device name
    NEED_ONLY_ADDR,          //only attention device MAC addr
    NEED_NONE,              //don't attention neither name nor addr
}search_mode;

typedef struct search
{
    char        name[32];
    uint8_t     addr[7];
    search_mode mode;
}search_target_t;

void ble_init(bool *erase_bonds);
uint32_t ble_act_central_send(void * const p_data, uint16_t length);
uint32_t ble_act_slave_send(void * const p_data, uint16_t length);
bool ble_connect_slave(uint16_t * const conn,const uint8_t * const p_mac,uint32_t timeout);
bool ble_disconnect_slave(uint16_t conn);
bool ble_scan_slave(uint32_t timeout);
uint8_t get_ble_s_max_data_len(void);
bool get_s_connect_status(void);
bool get_c_connect_status(void);
void scan_start(void);
void scan_stop(void);
uint8_t get_device_mac_addr(uint8_t * const p_mac, uint8_t const size);
bool scan_start_name(const char * const p_name);
bool ble_search_slave(search_target_t *slave, int32_t timeout);
#endif //BLE_DRUID_SVC_H
